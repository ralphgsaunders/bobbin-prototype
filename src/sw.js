const DEBUG = true
const { assets } = global.serviceWorkerOption
const cachedAssets = [...assets, './'].map(p => new URL(p, global.location).toString())
const CACHE_NAME = new Date().toISOString()

/**
 * Install the service worker
 *
 * We cache under the pathname to the service worker. This is because each build
 * has a unique chunk name and this allows us to build unique caches per sw.
 */
const install = event => {
    event.waitUntil(
        global.caches
            .open(CACHE_NAME)
            .then(cache => cache.addAll(cachedAssets))
            .then(() => DEBUG ? console.warn(`Cached Assets: ${cachedAssets}`) : null)
            .catch(error => console.error(error))
    )
}

/**
 * Activate the service worker
 *
 * Routine will clear old caches
 */
const activate = event => {
    event.waitUntil(
        global.caches.keys().then(names => Promise.all(
            names.map(name => {
                if(name.indexOf(CACHE_NAME) === 0) {
                    return null
                }

                return global.caches.delete(name)
            })
        ))
    )
}

self.addEventListener( 'install', install );
self.addEventListener( 'activate', activate );
