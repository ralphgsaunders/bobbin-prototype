import runtime from 'serviceworker-webpack-plugin/lib/runtime';
import FeedController from './feed-controller';
import feedTemplate from './templates/feed.html';

const target = document.querySelector( '.js-inject-target' )
const feedController = new FeedController();
feedController.fetchFeed( 'https://daringfireball.net/feeds/main' )
    .then(response => {
        const html = feedTemplate(response)
        target.innerHTML = html;
    });

if ('serviceWorker' in navigator) {
  runtime.register();
}
