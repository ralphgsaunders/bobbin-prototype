class FeedController {
    /**
     * Fetch Feed
     *
     * returns Promise
     */
    fetchFeed( url ) {
        return feednami.load( url )
    }
}

export default FeedController;
