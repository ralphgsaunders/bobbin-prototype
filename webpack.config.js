const path = require( 'path' );
const HtmlWebpackPlugin = require( 'html-webpack-plugin' );
const WebpackMd5Hash = require( 'webpack-md5-hash' );
const MiniCssExtractPlugin = require( 'mini-css-extract-plugin' );
const CleanWebpackPlugin = require( 'clean-webpack-plugin' );
const ServiceWorkerWebpackPlugin = require( 'serviceworker-webpack-plugin' );

module.exports = {
    entry: {
        main: './src/index.js',
        serviceWorker: './src/sw.js',
        vendor: [
            'underscore'
        ]
    },
    output: {
        filename: '[name].[chunkhash].js',
        path: path.resolve(__dirname, 'dist')
    },
    module: {
        rules: [
            {
                enforce: 'pre',
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'eslint-loader',
                options: {
                    fix: true
                }
            },
            {
                test: /\.js$/,
                exclude: [
                    /node_modules/,
                    /src\/sw.js$/
                ],
                loader: 'babel-loader'
            },
            {
                test: require.resolve('underscore'),
                use: [{
                    loader: 'expose-loader',
                    options: '_'
                }]
            },
            {
                test: /\.scss$/,
                use: [ 'style-loader', MiniCssExtractPlugin.loader,
                'css-loader', 'postcss-loader', 'sass-loader' ]
            },
            {
                test: /\.html$/,
                loader: 'underscore-template-loader'
            }
        ]
    },
    plugins: [
        new CleanWebpackPlugin('dist', {}),
        new MiniCssExtractPlugin({
            filename: 'style.[contenthash].css'
        }),
        new HtmlWebpackPlugin({
            inject: false,
            hash: true,
            template: './src/index.html',
            filename: 'index.html'
        }),
        new WebpackMd5Hash(),
        new ServiceWorkerWebpackPlugin({
            entry: path.join(__dirname, 'src/sw.js')
        })
    ],
    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        port: 9000
    }
};
